var socks = require('socksv5');

var srv = socks.createServer(function(info, accept, deny) {
  accept();
});

srv.listen(9999, '0.0.0.0', function() {
  console.log('SOCKS server listening on port 9999');
});

srv.useAuth(socks.auth.None());